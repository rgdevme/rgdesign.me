import Head from '../components/head'
import Hero from '../components/hero'
import Menu from '../components/menu';

export default function Home() {
  return (
    <>
      <Head />
      <Hero full bg='/herobg.webp' />
    </>
  );
}
