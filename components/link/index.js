import NextLink from 'next/link';

const Link = ({ as, href, label = 'Link', children }) => (
  <NextLink href={href} as={as} passHref>
    <a aria-label={label} className='py-2 px-4'>
      {children}
    </a>
  </NextLink>
);

export default Link;
