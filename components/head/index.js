import NextHead from 'next/head';

const Head = ({
  title = 'rgdesign.me',
  children,
  noindex = true,
  nofollow = true,
  featured_img
}) => {
  return (
    <NextHead>
      {/* Primary Meta Tags */}
      <meta charSet='utf-8' />
      <link rel="manifest" href="/manifest.json" />
      <meta name="theme-color" content="#222222" />
      <meta httpEquiv='Content-Type' content='text/html; charset=utf-8' />
      <meta name='viewport' content='width=device-width, initial-scale=1.0' />
      <meta
        name='robots'
        content={`${!noindex ? '' : 'no'}index ${!nofollow ? '' : 'no'}follow`}
      />

      {process.browser && window.matchMedia('(prefers-color-scheme: light)').matches ? (
        <link rel='icon' href='/favicon.png' />
      ) : (
        <link rel='icon' href='/favicon_dark.png' />
      )}

      <title>{title}</title>
      <meta name='title' content={title} />
      <meta name='description' content={children || 'rgdesign.me'} />

      {/* Open Graph / Facebook */}
      <meta property='og:type' content='website' />
      <meta property='og:url' content='https://metatags.io/' />
      <meta property='og:title' content={title} />
      <meta property='og:description' content={children} />
      <meta property='og:image' content={featured_img} />

      {/* Twitter */}
      <meta property='twitter:card' content='summary_large_image' />
      <meta property='twitter:url' content='https://metatags.io/' />
      <meta property='twitter:title' content={title} />
      <meta property='twitter:description' content={children} />
      <meta property='twitter:image' content={featured_img} />
    </NextHead>
  );
};

export default Head;
