import { useState } from 'react';
import tw, { css } from 'twin.macro';
import Button from '../buton';
import { Input } from '../form';
import apicall from '../utils/apicall';

const Hero = ({ bg, full }) => {
  const [formData, setFormData] = useState({
    to_email: '',
    to_name: '',
    service: '',
    buster: ''
  });

  const handleChange = (name, value) => {
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleSubmission = async (e) => {
    e.preventDefault();
    console.log(formData);
    const res = await apicall({
      data: formData,
      endpoint: '/contact',
      method: 'POST'
    });
  };

  return (
    <div
      css={css`
        ${tw`
          w-full 
          items-center
          content-center
          justify-center
          justify-items-center
          bg-center
          bg-cover
        `}

        display: grid;
        grid-template:
          'logo'
          'heading'
          'form'
          'text'
          / 1fr;
        gap: 1rem;

        height: ${full ? '100' : '85'}vh;
        background-image: url('${bg}');
        background-color: rgba(255 255 255 / 85%);
        background-blend-mode: color;
      `}
    >
      <img
        css={css`
          ${tw`
            h-32
            mb-8

            object-contain
            object-center
          `}

          grid-area: logo;
        `}
        src='/logo.svg'
        height='128'
        width='197'
        alt='RGDesign.me'
      />

      <h1
        css={css`
          grid-area: heading;
        `}
      >
        WEB DEV & Design
      </h1>

      <form
        id='contact'
        css={css`
          ${tw`
            content-center
            flex
            flex-col
            items-center
            justify-center
            justify-items-center
            w-3/5
            min-w-min
          `}

          grid-area: form;

          display: grid;
          grid-template:
            'services'
            'form'
            / 1fr;
          gap: 1rem;
        `}
      >
        <div
          css={css`
            ${tw`
              content-center
              flex
              items-center
              justify-center
            `}
            grid-area: services;
          `}
        >
          <span
            css={css`
              ${tw`
                text-primary
                uppercase
                text-3xl
                font-bold
                mr-6
              `}
            `}
          >
            Let's talk
          </span>
          <label className='no-access' htmlFor='select-service'>
            Choose a service
          </label>
          <div
            css={css`
              ${tw`
                relative
                inline
              `}
            `}
          >
            <span
              css={css`
                ${tw`
                  absolute
                  bg-center
                  bg-contain
                  bg-no-repeat
                  h-4
                  pointer-events-none
                  right-8
                  text-white
                  top-1/2
                  transform
                  translate-x-1/2
                  -translate-y-1/2
                  w-4
                  z-10
                `}

                background-image: url('/icons/caret-d.svg')
              `}
            />
            <select
              name='service'
              onLoad={({ target }) => handleChange(target.name, target.value)}
              onChange={({ target }) => handleChange(target.name, target.value)}
              css={css`
                ${tw`
                  appearance-none	
                  bg-secondary
                  font-bold
                  px-4
                  pr-16
                  py-2
                  relative
                  text-3xl
                  text-white
                  uppercase
                `}

                option {
                  ${tw`
                    font-bold
                    px-4
                    text-2xl
                    text-gray-lighter
                    uppercase
                  `}
                }
              `}
            >
              <option value='Multiple services'>Multiple services</option>
              <option value='Branding'>Branding</option>
              <option value='Web development'>Web development</option>
              <option value='App development'>App development</option>
            </select>
          </div>
        </div>
        <div
          css={css`
            ${tw`
              flex
              flex-row
              p-2
              justify-center
              content-center
              bg-white
              rounded-lg
              w-full
            `}

            grid-area: form;

            box-shadow: 0 0.5rem 1rem 0 rgba(0 0 0 / 15%);
          `}
        >
          <Input
            defaultValue=''
            formState={formData}
            form='contact'
            type='text'
            id='to_name'
            label='My name is...'
            placeholder='My name is...'
            onChange={handleChange}
            required
          />

          <Input
            defaultValue=''
            formState={formData}
            form='contact'
            type='email'
            id='to_email'
            label='You can reach me at....'
            placeholder='You can reach me at....'
            onChange={handleChange}
            required
          />

          <Input
            formState={formData}
            form='contact'
            onChange={handleChange}
            buster
          />

          <Button cta label='Submit form' onClick={handleSubmission}>
            Let's talk →
          </Button>
        </div>
      </form>
      <p>I develop elegant websites, that convert and boost SMBs sales.</p>
      <ul
        css={css`
          ${tw`
            list-none
            text-gray-lighter
            max-w-xs
          `}

          li {
            ${tw`
              relative
              my-4
              flex
              items-center
              content-center
            `}

            div {
              ${tw`
                w-fit
              `}
            }

            &::before {
              ${tw`
                inline-block
                h-8
                w-8
                mr-4
                bg-contain
                bg-no-repeat
                bg-center
              `}

              content: '';
            }
          }
        `}
      >
        <li
          css={css`
            &::before {
              background-image: url('/icons/stars.svg');
            }
          `}
        >
          <div>Elegant and scalable interfaces</div>
        </li>
        <li
          css={css`
            &::before {
              background-image: url('/icons/store.svg');
            }
          `}
        >
          <div>Get exposed! Boost your sales!</div>
        </li>
        <li
          css={css`
            &::before {
              background-image: url('/icons/handshake.svg');
            }
          `}
        >
          <div>Trust, transparency and quality</div>
        </li>
        <li
          css={css`
            &::before {
              background-image: url('/icons/new.svg');
            }
          `}
        >
          <div>
            Use modern technologies that help you scale your business and
            provide you with total control over your online presence.
          </div>
        </li>
      </ul>
    </div>
  );
};

export default Hero;
